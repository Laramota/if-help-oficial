<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlAluno */

$this->title = 'Update Ll Aluno: ' . $model->Aid;
$this->params['breadcrumbs'][] = ['label' => 'Ll Alunos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Aid, 'url' => ['view', 'id' => $model->Aid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ll-aluno-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
