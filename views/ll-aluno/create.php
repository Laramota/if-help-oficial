<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlAluno */

$this->title = 'Create Ll Aluno';
$this->params['breadcrumbs'][] = ['label' => 'Ll Alunos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ll-aluno-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
