<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use app\models\LlAluno;

/* @var $this yii\web\View */
/* @var $model app\models\LlDesabafo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ll-desabafo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Aid_fk')->
       dropDownList(ArrayHelper::map(LlAluno::find()
           ->orderBy('nome')
           ->all(),'Aid','nome'),
           ['prompt' => 'Selecione um paciente'] )  
    ?>

    <?= $form->field($model, 'data')->textInput() ?>

    <?= $form->field($model, 'texto')->textInput(['maxlength' => true]) ?>

    
    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
