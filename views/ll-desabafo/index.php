<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LlDesabafoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ll Desabafos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ll-desabafo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Desabafar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Did',
            'data',
            'texto',
            'Aid_fk',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
