<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlDesabafo */

$this->title = 'Update Ll Desabafo: ' . $model->Did;
$this->params['breadcrumbs'][] = ['label' => 'Ll Desabafos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Did, 'url' => ['view', 'Did' => $model->Did, 'data' => $model->data]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ll-desabafo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
