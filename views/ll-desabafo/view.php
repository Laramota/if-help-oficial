<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LlDesabafo */

$this->title = $model->Did;
$this->params['breadcrumbs'][] = ['label' => 'Ll Desabafos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ll-desabafo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'Did' => $model->Did, 'data' => $model->data], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'Did' => $model->Did, 'data' => $model->data], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Did',
            'data',
            'texto',
            'Aid_fk',
        ],
    ]) ?>

</div>
