<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlDesabafo */

$this->title = 'Create Ll Desabafo';
$this->params['breadcrumbs'][] = ['label' => 'Ll Desabafos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ll-desabafo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
