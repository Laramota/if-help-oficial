<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Seja bem vindo ao IF HELP!</h1>

        <p class="lead">Sinta se à vontade para um desabafo.</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Peça Ajuda</h2>

                <p>Ligue para 188 ( Centro de Valorização da vida).</p>

            
            </div>
            <div class="col-lg-4">
                <h2>Todos os dias é dia de falar sobre</h2>

                <p><img src="https://www.traduzca.com/wp-content/uploads/2019/09/setembro-amarelo-conheca-a-origem-do-movimento-global.jpg" width="350px"></p>

                
            </div>
            <div class="col-lg-4">
                <h2>Nós nos importamos!</h2>

                <p>Segundo dados recolhidos em 2012 pela Organização Mundial de Saúde (OMS), mais de 800 mil pessoas tiram a própria vida todos os anos, sendo 75% destes indivíduos moradores de países de baixa e média renda. Estima-se que no mundo acontece um suicídio a cada 40 segundos.

Atualmente, o suicídio é a segunda principal causa de morte entre jovens com idades entre 15 e 29 anos. Todos os dias, pelo menos 32 brasileiros tiram suas próprias vidas. Todos esses números poderiam ser evitados ou reduzidos consideravelmente se existissem políticas eficazes de prevenção do suicídio.</p>

                
            </div>
        </div>

    </div>
</div>
