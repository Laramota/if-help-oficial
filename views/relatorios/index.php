<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatórios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h1><?= Html::encode($this->title) ?></h1>
   <?= Html::a('Quantidade de respostas por aluno', ['relatorio1'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Quantidade de consultas por aluno', ['relatorio2'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Quantidade de desabafos por aluno', ['relatorio3'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Quantidade de respostas por perguntas', ['relatorio4'], ['class' => 'btn btn-success']) ?>

 
</div>