<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlPerguntas */

$this->title = 'Create Ll Perguntas';
$this->params['breadcrumbs'][] = ['label' => 'Ll Perguntas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ll-perguntas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
