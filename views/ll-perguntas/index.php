<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LlPerguntasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ll Perguntas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ll-perguntas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Teste', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Pid',
            'texto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
