<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlPerguntas */

$this->title = 'Update Ll Perguntas: ' . $model->Pid;
$this->params['breadcrumbs'][] = ['label' => 'Ll Perguntas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Pid, 'url' => ['view', 'id' => $model->Pid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ll-perguntas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
