<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use app\models\LlPerguntas;


/* @var $this yii\web\View */
/* @var $model app\models\LlRespostas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ll-respostas-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?= $form->field($model, 'Pid_fk')->
       dropDownList(ArrayHelper::map(LlPerguntas::find()
           ->orderBy('texto')
           ->all(),'Pid','texto'),
           ['prompt' => 'Selecione uma pergunta'] )
    ?>


    <?= $form->field($model, 'texto')->textInput(['maxlength' => true]) ?>



    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
