<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlRespostas */

$this->title = 'Create Ll Respostas';
$this->params['breadcrumbs'][] = ['label' => 'Ll Respostas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ll-respostas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
