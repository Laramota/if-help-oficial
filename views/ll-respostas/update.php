<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlRespostas */

$this->title = 'Update Ll Respostas: ' . $model->Rid;
$this->params['breadcrumbs'][] = ['label' => 'Ll Respostas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Rid, 'url' => ['view', 'id' => $model->Rid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ll-respostas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
