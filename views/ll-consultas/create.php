<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlConsultas */

$this->title = 'Create Ll Consultas';
$this->params['breadcrumbs'][] = ['label' => 'Ll Consultas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ll-consultas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
