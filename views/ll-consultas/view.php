<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LlConsultas */

$this->title = $model->Cid;
$this->params['breadcrumbs'][] = ['label' => 'Ll Consultas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ll-consultas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'Cid' => $model->Cid, 'data' => $model->data], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'Cid' => $model->Cid, 'data' => $model->data], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Cid',
            'horario',
            'data',
            'Aid_fk',
        ],
    ]) ?>

</div>
