<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlConsultas */

$this->title = 'Update Ll Consultas: ' . $model->Cid;
$this->params['breadcrumbs'][] = ['label' => 'Ll Consultas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Cid, 'url' => ['view', 'Cid' => $model->Cid, 'data' => $model->data]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ll-consultas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
