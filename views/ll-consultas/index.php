<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LlConsultasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ll Consultas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ll-consultas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Marcar Consulta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Cid',
            'horario',
            'data',
            'Aid_fk',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
