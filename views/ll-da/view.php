<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LlDa */

$this->title = $model->Rid_fk;
$this->params['breadcrumbs'][] = ['label' => 'Ll Das', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ll-da-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'Rid_fk' => $model->Rid_fk, 'Aid_fk' => $model->Aid_fk, 'data' => $model->data], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'Rid_fk' => $model->Rid_fk, 'Aid_fk' => $model->Aid_fk, 'data' => $model->data], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Rid_fk',
            'Aid_fk',
            'data',
        ],
    ]) ?>

</div>
