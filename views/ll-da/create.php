<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlDa */

$this->title = 'Criar Respostas';
$this->params['breadcrumbs'][] = ['label' => 'Ll Das', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ll-da-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
