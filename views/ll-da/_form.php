<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use app\models\LlRespostas;

use app\models\LlAluno;

/* @var $this yii\web\View */
/* @var $model app\models\LlDa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ll-da-form">

    <?php $form = ActiveForm::begin(); ?>
    

    <?= $form->field($model, 'Rid_fk')->
       dropDownList(ArrayHelper::map(LlRespostas::find()
           ->orderBy('texto')
           ->all(),'Rid','texto'),
           ['prompt' => 'Selecione um resposta'] )
    ?>


    <?= $form->field($model, 'Aid_fk')->
       dropDownList(ArrayHelper::map(LlAluno::find()
           ->orderBy('nome')
           ->all(),'Aid','nome'),
           ['prompt' => 'Selecione um paciente'] )  
    ?>


    <?= $form->field($model, 'data')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
