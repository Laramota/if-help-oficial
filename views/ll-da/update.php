<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LlDa */

$this->title = 'Update Ll Da: ' . $model->Rid_fk;
$this->params['breadcrumbs'][] = ['label' => 'Ll Das', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Rid_fk, 'url' => ['view', 'Rid_fk' => $model->Rid_fk, 'Aid_fk' => $model->Aid_fk, 'data' => $model->data]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ll-da-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
