<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ll_aluno".
 *
 * @property int $Aid
 * @property string $nome
 * @property string $email
 * @property int $tel
 *
 * @property LlConsultas[] $llConsultas
 * @property LlDesabafo[] $llDesabafos
 */
class LlAluno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'll_aluno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tel'], 'integer'],
            [['nome'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Aid' => 'Aid',
            'nome' => 'Nome',
            'email' => 'E-mail',
            'tel' => 'Telefone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLlConsultas()
    {
        return $this->hasMany(LlConsultas::className(), ['Aid_fk' => 'Aid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLlDesabafos()
    {
        return $this->hasMany(LlDesabafo::className(), ['Aid_fk' => 'Aid']);
    }
}
