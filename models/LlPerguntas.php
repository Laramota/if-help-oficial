<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ll_perguntas".
 *
 * @property int $Pid
 * @property string $texto
 *
 * @property LlRespostas[] $llRespostas
 */
class LlPerguntas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'll_perguntas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Pid' => 'Pid',
            'texto' => 'Texto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLlRespostas()
    {
        return $this->hasMany(LlRespostas::className(), ['Pid_fk' => 'Pid']);
    }
}
