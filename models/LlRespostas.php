<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ll_respostas".
 *
 * @property int $Rid
 * @property string $texto
 * @property int $Pid_fk
 *
 * @property LlPerguntas $pidFk
 */
class LlRespostas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'll_respostas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Pid_fk'], 'integer'],
            [['texto'], 'string', 'max' => 100],
            [['Pid_fk'], 'exist', 'skipOnError' => true, 'targetClass' => LlPerguntas::className(), 'targetAttribute' => ['Pid_fk' => 'Pid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Rid' => 'resposta',
            'texto' => 'Texto',
            'Pid_fk' => 'pergunta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPidFk()
    {
        return $this->hasOne(LlPerguntas::className(), ['Pid' => 'Pid_fk']);
    }
}
