<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ll_consultas".
 *
 * @property int $Cid
 * @property int $horario
 * @property string $data
 * @property int $Aid_fk
 *
 * @property LlAluno $aidFk
 */
class LlConsultas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'll_consultas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['horario', 'Aid_fk'], 'integer'],
            [['data'], 'required'],
            [['data'], 'safe'],
            [['Aid_fk'], 'exist', 'skipOnError' => true, 'targetClass' => LlAluno::className(), 'targetAttribute' => ['Aid_fk' => 'Aid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Cid' => 'Consulta',
            'horario' => 'Horário',
            'data' => 'Data',
            'Aid_fk' => 'Aluno',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAidFk()
    {
        return $this->hasOne(LlAluno::className(), ['Aid' => 'Aid_fk']);
    }
}
