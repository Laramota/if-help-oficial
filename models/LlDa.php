<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ll_da".
 *
 * @property int $Rid_fk
 * @property int $Aid_fk
 * @property string $data
 *
 * @property LlRespostas $ridFk
 * @property LlAluno $aidFk
 */
class LlDa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'll_da';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Rid_fk', 'Aid_fk', 'data'], 'required'],
            [['Rid_fk', 'Aid_fk'], 'integer'],
            [['data'], 'safe'],
            [['Rid_fk', 'Aid_fk', 'data'], 'unique', 'targetAttribute' => ['Rid_fk', 'Aid_fk', 'data']],
            [['Rid_fk'], 'exist', 'skipOnError' => true, 'targetClass' => LlRespostas::className(), 'targetAttribute' => ['Rid_fk' => 'Rid']],
            [['Aid_fk'], 'exist', 'skipOnError' => true, 'targetClass' => LlAluno::className(), 'targetAttribute' => ['Aid_fk' => 'Aid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Rid_fk' => 'resposta',
            'Aid_fk' => 'aluno',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRidFk()
    {
        return $this->hasOne(LlRespostas::className(), ['Rid' => 'Rid_fk']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAidFk()
    {
        return $this->hasOne(LlAluno::className(), ['Aid' => 'Aid_fk']);
    }
}
