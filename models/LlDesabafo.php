<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ll_desabafo".
 *
 * @property int $Did
 * @property string $data
 * @property string $texto
 * @property int $Aid_fk
 *
 * @property LlAluno $aidFk
 */
class LlDesabafo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'll_desabafo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data'], 'required'],
            [['data'], 'safe'],
            [['Aid_fk'], 'integer'],
            [['texto'], 'string', 'max' => 100],
            [['Aid_fk'], 'exist', 'skipOnError' => true, 'targetClass' => LlAluno::className(), 'targetAttribute' => ['Aid_fk' => 'Aid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Did' => 'Did',
            'data' => 'Data',
            'texto' => 'Texto',
            'Aid_fk' => 'Aluno',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAidFk()
    {
        return $this->hasOne(LlAluno::className(), ['Aid' => 'Aid_fk']);
    }
}
