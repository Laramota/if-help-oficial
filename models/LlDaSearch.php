<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LlDa;

/**
 * LlDaSearch represents the model behind the search form of `app\models\LlDa`.
 */
class LlDaSearch extends LlDa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Rid_fk', 'Aid_fk'], 'integer'],
            [['data'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LlDa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Rid_fk' => $this->Rid_fk,
            'Aid_fk' => $this->Aid_fk,
            'data' => $this->data,
        ]);

        return $dataProvider;
    }
}
