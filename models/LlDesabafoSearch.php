<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LlDesabafo;

/**
 * LlDesabafoSearch represents the model behind the search form of `app\models\LlDesabafo`.
 */
class LlDesabafoSearch extends LlDesabafo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Did', 'Aid_fk'], 'integer'],
            [['data', 'texto'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LlDesabafo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Did' => $this->Did,
            'data' => $this->data,
            'Aid_fk' => $this->Aid_fk,
        ]);

        $query->andFilterWhere(['like', 'texto', $this->texto]);

        return $dataProvider;
    }
}
