<?php

namespace app\controllers;

use Yii;
use app\models\LlDesabafo;
use app\models\LlDesabafoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LlDesabafoController implements the CRUD actions for LlDesabafo model.
 */
class LlDesabafoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LlDesabafo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LlDesabafoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LlDesabafo model.
     * @param integer $Did
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($Did, $data)
    {
        return $this->render('view', [
            'model' => $this->findModel($Did, $data),
        ]);
    }

    /**
     * Creates a new LlDesabafo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LlDesabafo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Did' => $model->Did, 'data' => $model->data]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LlDesabafo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $Did
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($Did, $data)
    {
        $model = $this->findModel($Did, $data);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Did' => $model->Did, 'data' => $model->data]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LlDesabafo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $Did
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($Did, $data)
    {
        $this->findModel($Did, $data)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LlDesabafo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $Did
     * @param string $data
     * @return LlDesabafo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Did, $data)
    {
        if (($model = LlDesabafo::findOne(['Did' => $Did, 'data' => $data])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
