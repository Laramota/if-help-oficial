<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;

 
class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }
   public function actionRelatorio1()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT ll_ALUNO.NOME, COUNT(ll_RESPOSTAS.RID)AS QUANTIDADE
        FROM ll_ALUNO JOIN ll_da on ll_aluno.aid = ll_da.aid_fk
        join ll_RESPOSTAS ON ll_da.rid_fk = ll_RESPOSTAS.rid
        GROUP BY ll_ALUNO.AID
        ORDER BY COUNT(ll_RESPOSTAS.RID) DESC',
            ]
        );
    
        return $this->render('relatorio1', ['resultado' => $consulta]);
}
public function actionRelatorio2()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT ll_ALUNO.NOME, COUNT(ll_CONSULTAS.CID)AS QUANTIDADE
        FROM ll_ALUNO JOIN ll_consultas on ll_aluno.aid = ll_consultas.cid
        GROUP BY ll_ALUNO.AID
        ORDER BY COUNT(ll_CONSULTAS.CID) DESC',
            ]
        );
    
        return $this->render('relatorio2', ['resultado' => $consulta]);
}
public function actionRelatorio3()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT ll_ALUNO.NOME, COUNT(ll_desabafo.DID)AS QUANTIDADE
        FROM ll_ALUNO JOIN ll_desabafo on ll_aluno.aid = ll_desabafo.did
        GROUP BY ll_ALUNO.AID
        ORDER BY COUNT(ll_DESABAFO.DID) DESC',
            ]
        );
    
        return $this->render('relatorio3', ['resultado' => $consulta]);
}
public function actionRelatorio4()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT ll_respostas.TEXTO, COUNT(ll_perguntas.PID)AS QUANTIDADE
        FROM ll_RESPOSTAS JOIN ll_PERGUNTAS on ll_RESPOSTAS.Rid = ll_PERGUNTAS.Pid
        GROUP BY ll_RESPOSTAS.RID
        ORDER BY COUNT(ll_PERGUNTAS.PID) DESC',
            ]
        );
    
        return $this->render('relatorio4', ['resultado' => $consulta]);
}
}
?>