<?php

namespace app\controllers;

use Yii;
use app\models\LlDa;
use app\models\LlDaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LlDaController implements the CRUD actions for LlDa model.
 */
class LlDaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LlDa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LlDaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LlDa model.
     * @param integer $Rid_fk
     * @param integer $Aid_fk
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($Rid_fk, $Aid_fk, $data)
    {
        return $this->render('view', [
            'model' => $this->findModel($Rid_fk, $Aid_fk, $data),
        ]);
    }

    /**
     * Creates a new LlDa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LlDa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Rid_fk' => $model->Rid_fk, 'Aid_fk' => $model->Aid_fk, 'data' => $model->data]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LlDa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $Rid_fk
     * @param integer $Aid_fk
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($Rid_fk, $Aid_fk, $data)
    {
        $model = $this->findModel($Rid_fk, $Aid_fk, $data);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Rid_fk' => $model->Rid_fk, 'Aid_fk' => $model->Aid_fk, 'data' => $model->data]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LlDa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $Rid_fk
     * @param integer $Aid_fk
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($Rid_fk, $Aid_fk, $data)
    {
        $this->findModel($Rid_fk, $Aid_fk, $data)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LlDa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $Rid_fk
     * @param integer $Aid_fk
     * @param string $data
     * @return LlDa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Rid_fk, $Aid_fk, $data)
    {
        if (($model = LlDa::findOne(['Rid_fk' => $Rid_fk, 'Aid_fk' => $Aid_fk, 'data' => $data])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
