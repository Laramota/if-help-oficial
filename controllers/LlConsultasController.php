<?php

namespace app\controllers;

use Yii;
use app\models\LlConsultas;
use app\models\LlConsultasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LlConsultasController implements the CRUD actions for LlConsultas model.
 */
class LlConsultasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LlConsultas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LlConsultasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LlConsultas model.
     * @param integer $Cid
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($Cid, $data)
    {
        return $this->render('view', [
            'model' => $this->findModel($Cid, $data),
        ]);
    }

    /**
     * Creates a new LlConsultas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LlConsultas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Cid' => $model->Cid, 'data' => $model->data]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LlConsultas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $Cid
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($Cid, $data)
    {
        $model = $this->findModel($Cid, $data);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Cid' => $model->Cid, 'data' => $model->data]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LlConsultas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $Cid
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($Cid, $data)
    {
        $this->findModel($Cid, $data)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LlConsultas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $Cid
     * @param string $data
     * @return LlConsultas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Cid, $data)
    {
        if (($model = LlConsultas::findOne(['Cid' => $Cid, 'data' => $data])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
